<?php

/**
 * Plugin Name: Import Members
 */

if(!defined('ABSPATH')) {die('You are not allowed to call this page directly.');}


// add_action('init', 'import_members');
function import_members()
{
    // $files = new RecursiveIteratorIterator( new RecursiveDirectoryIterator(__DIR__));
    // foreach ($files as $file) :
    //     if ( $file->getExtension() == 'json') {

            $data = file_get_contents( __DIR__ . "/Lapsed and Active Members.csv.json" );
            $data = json_decode($data);

            foreach ($data as $member ) {
                import_member($member);
            }
    //     }
    // endforeach;
}
function import_member( $member ) {
    $name_first = $member->{'First Name'};
    $name_last = $member->{'Last Name'};
    $email = $member->{'Email'};
    $member_status = $member->{'Membership status'};
    $member_type = $member->{'Membership level'};
    $member_role = $member->{'Member role'};

    // Create User.
    $user = get_user_by('email', $email);
    if ( ! $user ) {
        $user = wp_insert_user([
            'user_pass' => wp_generate_password(),
            'user_login' => $email,
            'user_email' => $email,
            'first_name' => $name_first,
            'last_name' => $name_last,
        ]);

        if ( is_wp_error($user) ) {
            error_log( $user->get_error_message() );
            return;
        }
        $user = get_userdata( $user );

        // Create Member.
        $user = new MeprUser($user->ID);
        $user->update_member_data();
    }

    if ( ! is_a( $user, 'MeprUser' ) ) {
        $user = new MeprUser($user->ID);
    }

    // Create Membership.
    $products = MeprProduct::get_all();

    if ( ! in_array($member_type, array_column($products,'post_title')) || 'Active' !== $member_status ) {
        return;
    }

    $product = array_column($products, 'post_title' );
    $product = array_search($member_type, $product );
    $product = $products[$product];

    if ( $member_type == 'Individual Membership' ) {
        setup_individual( $user, $product );
    }

    if ( $member_type == 'Corporate Membership' ) {
        setup_corporate( $user, $product, $member );
    }

    return;
}

function setup_individual( MeprUser $user, MeprProduct $product ) {
    $trans = MeprTransaction::get_all_by_user_id($user->ID);
    $subs = $user->current_and_prior_subscriptions();
    $active_subs = $user->active_product_subscriptions('ids');

    // Create transaction with no active product.
    if (! $user->is_active_on_membership($product) ) {
        $product_id = $product->ID;
        $product_title = $product->post_title;

        $txn = new MeprTransaction();
        $txn->user_id = $user->ID;
        $txn->product_id = $product->ID;

        $price = $product->adjusted_price();
        $txn->set_subtotal($price);
        $txn->gateway = MeprTransaction::$manual_gateway_str;
        $txn->status = 'complete';

        $pm = $txn->payment_method();
        // Create a new subscription
        if($product->is_one_time_payment()) {
            $signup_type = 'non-recurring';
        } else {
            $signup_type = 'recurring';
  
            $sub = new MeprSubscription();
            $sub->user_id = $user->ID;
            $sub->gateway = $pm->id;

            $sub->maybe_prorate(); // sub to sub
            $sub->store();
  
            $txn->subscription_id = $sub->id;
        }
        
        $txn->store();

        MeprHooks::do_action("mepr-{$signup_type}-signup", $txn);
        MeprHooks::do_action('mepr-signup', $txn);
    } else {
        if ( $trans ) {
            // foreach( $trans as $t) {
            //     $tran = new MeprTransaction( $t->id );
            //     $tran->destroy();
            // }
        }

        if ( count( $subs ) > 1 ) {
            foreach( $subs as $sub ) {
                $sub = new MeprSubscription( $sub );
                if ( $sub->status == 'active' ) {

                }
            }
            // return;
        }
    }
}

function setup_corporate( MeprUser $user, MeprProduct $product, $member = [] ) {
    $trans = MeprTransaction::get_all_by_user_id($user->ID);
    $subs = $user->current_and_prior_subscriptions();
    $active_subs = $user->active_product_subscriptions('ids');

    // Corporate Admin.
    if ( $member->{'Member role'} == 'Bundle administrator' && ! $user->is_active_on_membership($product) ) {
        $product_id = $product->ID;
        $product_title = $product->post_title;

        $txn = new MeprTransaction();
        $txn->user_id = $user->ID;
        $txn->product_id = $product->ID;

        $price = $product->adjusted_price();
        $txn->set_subtotal($price);
        $txn->gateway = MeprTransaction::$manual_gateway_str;
        $txn->status = 'complete';

        $pm = $txn->payment_method();
        // Create a new subscription
        if($product->is_one_time_payment()) {
            $signup_type = 'non-recurring';
        } else {
            $signup_type = 'recurring';
  
            $sub = new MeprSubscription();
            $sub->user_id = $user->ID;
            $sub->gateway = $pm->id;

            $sub->maybe_prorate(); // sub to sub
            $sub->store();
  
            $txn->subscription_id = $sub->id;
        }
        
        $txn_id = $txn->store();

        MeprHooks::do_action("mepr-{$signup_type}-signup", $txn);
        MeprHooks::do_action('mepr-signup', $txn);
    }

    // Corporate Member.
    if ($member->{'Member role'} == 'Bundle member') {
        $admin_email = $member->{'Member bundle ID or email'};
        $ca_admin = new MeprUser();
        $ca_admin->load_user_data_by_email( $admin_email );
        if ( $ca_admin->is_active_on_membership($product) ) {
            $subs = $ca_admin->active_product_subscriptions('ids');

            $ca = new MPCA_Corporate_Account($subs[0]);
            // Associate the sub account user with the corporate account
            $result = $ca->add_sub_account_user( $user->ID );
        }
    }
}
